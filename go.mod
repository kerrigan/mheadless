module bitbuclet.org/kerrigan/mheadless

go 1.14

require (
	bitbucket.org/kerrigan/mheadless v0.0.0-20180926051609-7203e0f1a541
	layeh.com/gumble v0.0.0-20200528195103-d5a9569f0683
)
