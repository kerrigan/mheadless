package bot

import (
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"os"
	"time"

	"layeh.com/gumble/gumble"
	"layeh.com/gumble/gumbleopenal"
	"layeh.com/gumble/gumbleutil"
)

type MHeadless struct {
	Config *gumble.Config
	Client *gumble.Client

	TLSConfig tls.Config

	Stream *gumbleopenal.Stream

	Settings Settings
}

func (p *MHeadless) Init() {
	p.Config.Username = p.Settings.Nickname

	log.Println("Username", p.Config.Username)
	p.Config.Attach(gumbleutil.AutoBitrate)
	p.Config.Attach(p)

	p.Connect()
}

func (p *MHeadless) Connect() {

	address := fmt.Sprintf("%s:%d", p.Settings.Hostname, p.Settings.Port)
	_, err := gumble.DialWithDialer(new(net.Dialer), address, p.Config, &p.TLSConfig)

	if err != nil {
		log.Println("Failed to connect", err)
		p.Reconnect()
	} else {
		p.OpenStream()
	}
}

func (p *MHeadless) Reconnect() {
	if p.Client != nil {
		p.Client.Disconnect()
	}

	go func() {
		time.Sleep(3 * time.Second)
		p.Connect()
	}()
}

func (p *MHeadless) OpenStream() {
	if stream, err := gumbleopenal.New(p.Client); err != nil {
		log.Printf("Stream open error (%s)\n", err)
		os.Exit(1)
	} else {
		p.Stream = stream
	}
}

func (p *MHeadless) OnUserList(e *gumble.UserListEvent) {
}

func (p *MHeadless) OnACL(e *gumble.ACLEvent) {
}

func (p *MHeadless) OnBanList(e *gumble.BanListEvent) {
}

func (p *MHeadless) OnContextActionChange(e *gumble.ContextActionChangeEvent) {
}

func (p *MHeadless) OnServerConfig(e *gumble.ServerConfigEvent) {
}

func (p *MHeadless) OnTextMessage(e *gumble.TextMessageEvent) {
}

func (p *MHeadless) OnUserChange(e *gumble.UserChangeEvent) {

}

func (p *MHeadless) OnChannelChange(e *gumble.ChannelChangeEvent) {
}

func (p *MHeadless) OnPermissionDenied(e *gumble.PermissionDeniedEvent) {
	var info string
	switch e.Type {
	case gumble.PermissionDeniedOther:
		info = e.String
	case gumble.PermissionDeniedPermission:
		info = "insufficient permissions"
	case gumble.PermissionDeniedSuperUser:
		info = "cannot modify SuperUser"
	case gumble.PermissionDeniedInvalidChannelName:
		info = "invalid channel name"
	case gumble.PermissionDeniedTextTooLong:
		info = "text too long"
	case gumble.PermissionDeniedTemporaryChannel:
		info = "temporary channel"
	case gumble.PermissionDeniedMissingCertificate:
		info = "missing certificate"
	case gumble.PermissionDeniedInvalidUserName:
		info = "invalid user name"
	case gumble.PermissionDeniedChannelFull:
		info = "channel full"
	case gumble.PermissionDeniedNestingLimit:
		info = "nesting limit"
	}
	log.Println("Info: ", info)
}

func (p *MHeadless) OnConnect(e *gumble.ConnectEvent) {
	p.Client = e.Client

	channel := p.Client.Channels.Find(p.Settings.Channel)

	if channel != nil {
		p.Client.Self.Move(channel)
	}
}

func (p *MHeadless) OnDisconnect(e *gumble.DisconnectEvent) {
	//	log.Fatal("Disconnected")
	//for {
	time.Sleep(5 * time.Second)
	log.Println("Trying reconnect")
	//TODO: reconnect
	p.Reconnect()
	//}
}
