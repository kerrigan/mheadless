package bot

type Settings struct {
	Nickname string `json:"nickname"`
	Hostname string `json:"hostname"`
	Port     int    `json:"port"`
	Channel  string `json:"channel"`
}
