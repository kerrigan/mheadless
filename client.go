package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/json"
	"encoding/pem"
	"flag"
	"fmt"
	"log"
	"math/big"
	"os"
	"os/signal"
	"os/user"
	"runtime"
	"strconv"
	"syscall"
	"time"

	bot "bitbucket.org/kerrigan/mheadless/bot"

	"layeh.com/gumble/gumble"
	_ "layeh.com/gumble/opus"

	"runtime/pprof"
)

func runMemprofile() {
	var memprofile = "mem.prof"
	f, err := os.Create(memprofile)
	if err != nil {
		log.Fatal(err)
	}
	pprof.WriteHeapProfile(f)
}

func main() {
	CheckKeys()

	var configFileName = flag.String("c", "config.json", "config file name")
	flag.Parse()

	settings := loadSettings(*configFileName)
	//config.Address = fmt.Sprintf("%s:%d", settings.Hostname, settings.Port)

	mh := bot.MHeadless{
		Config:   gumble.NewConfig(),
		Settings: settings,
	}

	if cert, err := tls.LoadX509KeyPair("certs/client.pem", "certs/client.key"); err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	} else {
		mh.TLSConfig.Certificates = []tls.Certificate{cert}
	}

	mh.TLSConfig.InsecureSkipVerify = true
	mh.Init()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for sig := range c {
			log.Println(sig.String())
			// sig is a ^C, handle it
			//abot.Stream.Destroy()
			runMemprofile()
			os.Exit(0)
		}
	}()

	<-make(chan int)
}

func changeUser(username string) {

	runUser, err := user.Lookup(username)
	if err != nil {
		log.Println(err)
	} else {
		log.Println(runUser)

		gid, err := strconv.ParseInt(runUser.Gid, 10, 64)
		if err != nil {
			log.Fatal("Failed to parse uid")
		}

		runtime.LockOSThread()
		if en := syscall.Setgid(int(gid)); en != nil {
			log.Println("Setgid error:", en)
		} else {
			log.Println("User changed to:", runUser.Name)
		}
		runtime.UnlockOSThread()
	}
}

func IsFileExists(filename string) bool {
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return false
	}
	return true
}

func CheckKeys() {
	keyPath := "certs"
	keyFilename := keyPath + "/client.key"
	certFilename := keyPath + "/client.pem"

	if IsFileExists(keyPath) && IsFileExists(keyFilename) && IsFileExists(certFilename) {
		return
	}
	os.Mkdir("certs", 0755)

	fpriv, err := os.Create(keyFilename)
	if err != nil {
		log.Fatal(err)
	}

	fpub, err := os.Create(certFilename)

	if err != nil {
		log.Fatal(err)
	}

	size := 2048
	priv, err := rsa.GenerateKey(rand.Reader, size)

	if err != nil {
		log.Fatal(err)
	}

	template := &x509.Certificate{
		IsCA:                  true,
		BasicConstraintsValid: true,
		SubjectKeyId:          []byte{1, 2, 3},
		SerialNumber:          big.NewInt(1234),
		Subject: pkix.Name{
			Country:      []string{"Rochland"},
			Organization: []string{"Mother Nature"},
		},
		NotBefore: time.Now(),
		NotAfter:  time.Now().AddDate(30, 5, 5),
		// see http://golang.org/pkg/crypto/x509/#KeyUsage
		ExtKeyUsage: []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:    x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
	}

	var parent = template

	cert, err := x509.CreateCertificate(rand.Reader, template, parent, &priv.PublicKey, priv)

	if err != nil {
		log.Fatal(err)
	}

	pem.Encode(fpub, &pem.Block{Type: "CERTIFICATE", Bytes: cert})

	pem.Encode(fpriv,
		&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(priv),
		},
	)

	fmt.Println("New keypair created")

}

func loadSettings(filename string) bot.Settings {
	file, err := os.Open(filename)

	if err != nil {
		log.Fatal(err)
	}
	decoder := json.NewDecoder(file)

	config := bot.Settings{}
	err = decoder.Decode(&config)

	if err != nil {
		//return
		log.Fatal(err)
	}

	log.Printf("%+v\n", config)

	return config
}
